﻿using FileManager_WebApi.Attributes;
using FileManager_WebApi.Models;
using FileManager_WebApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using OneBitSoftware.Utilities;

namespace FileManager_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IContentManager<int> _manager;

        public FileController(IContentManager<int> manager)
        {
            _manager = manager;
        }

        [HttpPost("{id}")]
        [ServiceFilter(typeof(ExctentionFilter))]
        public async Task<IActionResult> SaveFile([FromRoute] int id, [FromForm] IFormFile file)
        {
            OperationResult result = default;
            if (file.Length > 0)
            {
                var model = new StreamInfo();
                using (var ms = new MemoryStream())
                {
                    string name = file.FileName;
                    file.CopyTo(ms);
                    var fileBytes = ms.ToArray();

                    model.Length = ms.Length;
                    model.Stream = new MemoryStream(fileBytes);
                }

                result = await _manager.StoreAsync(id, model, CancellationToken.None);
                await model.Stream.DisposeAsync();
                return Ok(result);
            }

            result = new OperationResult<string>();
            result.AddSuccessMessage("The file was empty!");

            return BadRequest(result);
        }

        [HttpGet("exist/{id}")]
        public async Task<IActionResult> FileExist([FromRoute] int id)
        {
            var result = await _manager.ExistsAsync(id, CancellationToken.None);

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            var file = await _manager.GetAsync(id, CancellationToken.None);
            var result = new OperationResult<byte[]>();
            if (file.ResultObject is not null)
            {
                using (var ms = new MemoryStream())
                {
                    await file.ResultObject.Stream.CopyToAsync(ms);
                    result.ResultObject = ms.ToArray();
                    result.AddSuccessMessage("File is converted from byte[] to string!");
                    return Ok(result);
                }
            }
            else
            {
                foreach (var error in file.Errors)
                {
                    result.AppendError(error.Message);
                }
            }

            return NotFound(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            var result = await _manager.DeleteAsync(id, CancellationToken.None);

            return Ok(result);
        }

        [HttpGet("bytes/{id}")]
        public async Task<IActionResult> GetAsBytes([FromRoute] int id)
        {
            var file = await _manager.GetBytesAsync(id, CancellationToken.None);

            if (file.ResultObject is not null)
            {
                return Ok(file);
            }

            return NotFound(file);
        }

        [HttpGet("hash/{id}")]
        public async Task<IActionResult> GetAsHashing([FromRoute] int id)
        {
            var file = await _manager.GetHashAsync(id, CancellationToken.None);

            if (file.ResultObject is not null)
            {
                return Ok(file);
            }

            return NotFound(file);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromForm] IFormFile file)
        {
            OperationResult result = default;
            if (file.Length > 0)
            {
                var model = new StreamInfo();
                using (var ms = new MemoryStream())
                {
                    string name = file.FileName;
                    file.CopyTo(ms);
                    var fileBytes = ms.ToArray();

                    model.Length = ms.Length;
                    model.Stream = new MemoryStream(fileBytes);

                    result = await _manager.UpdateAsync(id, model, CancellationToken.None);
                }

                if (result.Fail)
                {
                    return BadRequest(result);
                }

                return Ok(result);
            }

            result = new OperationResult();
            result.AppendError("The file was empty!");
            return BadRequest(result);
        }
    }
}