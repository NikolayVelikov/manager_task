using FileManager_WebApi.Attributes;
using FileManager_WebApi.Models;
using FileManager_WebApi.Services;
using FileManager_WebApi.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

var directory = Directory.GetCurrentDirectory();
var path = Path.Combine(directory, "Configurations");
builder.Configuration
       .SetBasePath(path)
       .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
       .AddJsonFile("connection.json", optional: false, reloadOnChange: true);

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IContentManager<int>, ContentManager<int>>();

builder.Services.AddTransient<ExctentionFilter>();

builder.Services.Configure<ConnectionServer>(builder.Configuration.GetSection(nameof(ConnectionServer)));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

public partial class Program { }