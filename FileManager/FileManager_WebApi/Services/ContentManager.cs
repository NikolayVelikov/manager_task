﻿using FileManager_WebApi.Models;
using FileManager_WebApi.Services.Interfaces;
using Microsoft.Extensions.Options;
using OneBitSoftware.Utilities;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace FileManager_WebApi.Services
{
    public class ContentManager<TKey> : IContentManager<TKey>
        where TKey : struct, IEquatable<TKey>
    {
        private readonly ConnectionServer _options;

        public ContentManager(IConfiguration configuration, IOptions<ConnectionServer> options)
        {
            _options = options.Value;
        }

        public async Task<OperationResult> DeleteAsync(TKey id, CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                var path = Path.Combine(_options.Connection, $"{id}{_options.FileFormat}");
                File.Delete(path);

            });

            var result = new OperationResult();
            result.AddSuccessMessage("File was deleted");
            return result;
        }

        public async Task<OperationResult<bool>> ExistsAsync(TKey id, CancellationToken cancellationToken)
        {
            var operationResult = new OperationResult<bool>();

            await Task.Run(() =>
            {
                var result = Directory.GetFiles(_options.Connection);
                if (result.Any(x => x.EndsWith($"{id}{_options.FileFormat}")))
                {
                    operationResult.ResultObject = true;
                }
                else
                {
                    operationResult.AppendError($"There is not a file with Id: {id}");
                }
            });

            return operationResult;
        }

        public async Task<OperationResult<StreamInfo>> GetAsync(TKey id, CancellationToken cancellationToken)
        {
            OperationResult<StreamInfo> result = default;

            var isExist = await ExistsAsync(id, cancellationToken);
            if (isExist.Fail)
            {
                result = new OperationResult<StreamInfo>();
                foreach (var item in isExist.Errors)
                {
                    result.AppendError(item.Message);
                }
                return result;
            }

            string path = Path.Combine(_options.Connection, $"{id}{_options.FileFormat}");
            var byteArray = await ReturnFileAsByteAsync(path);

            var obj = new StreamInfo();
            obj.Stream = new MemoryStream(byteArray);
            obj.Length = obj.Stream.Length;

            result = new OperationResult<StreamInfo>(obj);

            return result;
        }

        public async Task<OperationResult<byte[]>> GetBytesAsync(TKey id, CancellationToken cancellationToken)
        {
            OperationResult<byte[]> result = default;

            var isExist = await ExistsAsync(id, cancellationToken);
            if (isExist.Fail)
            {
                result = new OperationResult<byte[]>();
                foreach (var item in isExist.Errors)
                {
                    result.AppendError(item.Message);
                }
                return result;
            }

            string path = Path.Combine(_options.Connection, $"{id}{_options.FileFormat}");
            var byteArray = await ReturnFileAsByteAsync(path);
            result = new OperationResult<byte[]>(byteArray);

            return result;
        }

        public async Task<OperationResult<string>> GetHashAsync(TKey id, CancellationToken cancellationToken)
        {
            OperationResult<string> result = default;

            var isExist = await ExistsAsync(id, cancellationToken);
            if (isExist.Fail)
            {
                result = new OperationResult<string>();
                foreach (var item in isExist.Errors)
                {
                    result.AppendError(item.Message);
                }
                return result;
            }

            string path = Path.Combine(_options.Connection, $"{id}{_options.FileFormat}");
            var byteArray = await ReturnFileAsByteAsync(path);
            string hash = CreateHash(byteArray);

            result = new OperationResult<string>(hash);

            return result;
        }

        public async Task<OperationResult> StoreAsync(TKey id, StreamInfo fileContent, CancellationToken cancellationToken)
        {
            var exist = await ExistsAsync(id, cancellationToken);
            if (!exist.Success)
            {
                return exist;
            }

            using (var fileStream = File.Create(_options.Connection + $"{id}{_options.FileFormat}"))
            {
                fileContent.Stream.Seek(0, SeekOrigin.Begin);
                await fileContent.Stream.CopyToAsync(fileStream);
            }

            var result = new OperationResult<string>();
            result.AddSuccessMessage("File is stored");

            return result;
        }


        public async Task<OperationResult> UpdateAsync(TKey id, StreamInfo fileContent, CancellationToken cancellationToken)
        {
            var isExist = await ExistsAsync(id, cancellationToken);
            if (!isExist.Success)
            {
                return isExist;
            }

            string inputFileContent = string.Empty;
            using (StreamReader reader = new StreamReader(fileContent.Stream))
            {
                inputFileContent = await reader.ReadToEndAsync();
            }

            string path = Path.Combine(_options.Connection, $"{id}{_options.FileFormat}");
            using (TextWriter tw = new StreamWriter(path, true))
            {
                await tw.WriteAsync(inputFileContent);
            }

            var result = new OperationResult();
            result.AddSuccessMessage("File is updated");
            return result;
        }

        private async Task<byte[]> ReturnFileAsByteAsync(string path)
        {
            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var sr = new StreamReader(fs, Encoding.UTF8))
                {
                    string content = await sr.ReadToEndAsync();

                    byte[] byteArray = Encoding.UTF8.GetBytes(content);

                    return byteArray;
                }
            }
        }

        private string CreateHash(byte[] array)
        {
            StringBuilder sb = new StringBuilder(array.Length);
            for (int i = 0; i < array.Length; i++)
            {
                sb.Append(array[i].ToString("X2"));
            }

            return sb.ToString();
        }
    }
}