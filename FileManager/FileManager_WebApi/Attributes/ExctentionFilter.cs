﻿using FileManager_WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using OneBitSoftware.Utilities;

namespace FileManager_WebApi.Attributes
{
    public class ExctentionFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var operationResult = new OperationResult<string>();
            var fileIsAttached = context.ActionArguments.TryGetValue("file", out object value);
            if (!fileIsAttached)
            {
                operationResult.AppendError("File is missing!");
                context.Result = new BadRequestObjectResult(operationResult);
            }
            else if (value is not IFormFile)
            {
                operationResult.AppendError("File is not in correct type format!");
                context.Result = new BadRequestObjectResult(operationResult);
            }

            var file = value as IFormFile;
            var fileName = file.FileName;
            var fileExtension = Path.GetExtension(fileName);

            var extension = context.HttpContext
                                   .RequestServices.GetRequiredService<IOptions<ConnectionServer>>()?
                                   .Value?
                                   .FileFormat;

            if (fileExtension.Equals(extension))
            {
                await next();
            }
            else
            {
                operationResult.AppendError($"File extension is not as {extension}!");
                context.Result = new BadRequestObjectResult(operationResult);
            }
        }
    }
}