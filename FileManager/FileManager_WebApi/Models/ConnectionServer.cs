﻿namespace FileManager_WebApi.Models
{
    public class ConnectionServer
    {
        private string _fileFormat;
        public string Connection { get; set; }

        public string FileFormat
        {
            get
            {
                return _fileFormat;
            }
            set
            {
                if (!value.StartsWith('.'))
                {
                    _fileFormat = $".{value}";
                }
                else
                {
                    _fileFormat = value;
                }
            }
        }
    }
}