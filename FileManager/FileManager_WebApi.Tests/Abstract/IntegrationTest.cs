﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FileManager_WebApi.Tests.Abstract
{
    public abstract class IntegrationTest
    {
        protected readonly HttpClient TestClient;
        protected readonly string BasePath;

        protected IntegrationTest()
        {
            var directory = Directory.GetCurrentDirectory();

            var parent = Directory.GetParent(directory).FullName;
            while (!parent.EndsWith("FileManager_WebApi.Tests"))
            {
                var current = Directory.GetParent(parent);
                parent = current.FullName;
            }

            BasePath = parent;
            string basePath = Path.Combine(parent, "Configurations");

            var factory = new WebApplicationFactory<Program>().WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    services.Configure<ConfigurationManager>(options =>
                    {
                        options.SetBasePath(basePath);
                        options.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                        options.AddJsonFile("connection.json", optional: false, reloadOnChange: true);
                    });
                });
            });

            TestClient = factory.CreateClient();
            TestClient.DefaultRequestHeaders.Add("Accept", "application/json");
        }
    }
}
