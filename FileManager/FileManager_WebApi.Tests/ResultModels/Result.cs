﻿namespace FileManager_WebApi.Tests.ResultModels
{
    public class Result<T>
    {
        public T? ResultObject { get; set; }

        public bool Success { get; set; }

        public string[] SuccessMessages { get; set; }

        public List<Error> Errors { get; set; }
    }

    public class Error
    {
        public string Message { get; set; }
    }
}