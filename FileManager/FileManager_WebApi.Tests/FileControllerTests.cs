using FileManager_WebApi.Tests.Abstract;
using FileManager_WebApi.Tests.ResultModels;
using FluentAssertions;
using Newtonsoft.Json;
using System.Net;
using Xunit;

namespace FileManager_WebApi.Tests
{
    public class FileControllerTests : IntegrationTest
    {
        public FileControllerTests()
        : base()
        { }

        [Fact]
        public async Task Should_Return_True_When_File_Exist()
        {
            HttpResponseMessage result = await TestClient.GetAsync("api/file/exist/5");

            var model = await result.Content.ReadAsStringAsync();
            var actualObj = JsonConvert.DeserializeObject<Result<bool>>(model);

            result.StatusCode.Should().Be(HttpStatusCode.OK);
            actualObj.Should().NotBeNull();
            actualObj.Success.Should().BeTrue();
            actualObj.SuccessMessages.Should().BeNull();
            actualObj.Errors.Count().Should().Be(0);
        }

        [Fact]
        public async Task Should_Return_False_When_File_Not_Exist()
        {
            HttpResponseMessage result = await TestClient.GetAsync("api/file/exist/15");

            var model = await result.Content.ReadAsStringAsync();
            var actualObj = JsonConvert.DeserializeObject<Result<bool>>(model);

            result.StatusCode.Should().Be(HttpStatusCode.OK);
            actualObj.Should().NotBeNull();
            actualObj.Success.Should().BeFalse();
            actualObj.SuccessMessages.Should().BeNull();
            actualObj.Errors.Count().Should().Be(1);
            actualObj.Errors[0].Message.Should().Be("There is not a file with Id: 15");
        }

        [Fact]
        public async Task Should_Return_File_When_It_Exist()
        {
            HttpResponseMessage result = await TestClient.GetAsync("api/file/5");

            var model = await result.Content.ReadAsStringAsync();
            var actualObj = JsonConvert.DeserializeObject<Result<string>>(model);

            result.StatusCode.Should().Be(HttpStatusCode.OK);
            actualObj.Should().NotBeNull();
            actualObj.Success.Should().BeTrue();
            actualObj.ResultObject.Should().Be("RGVtbyBUZXN0");
            actualObj.SuccessMessages.Length.Should().Be(1);
            actualObj.SuccessMessages[0].Should().Be("File is converted from byte[] to string!");
        }

        [Fact]
        public async Task Should_Return_NotFound_When_File_Not_Exist()
        {
            HttpResponseMessage result = await TestClient.GetAsync("api/file/15");

            var model = await result.Content.ReadAsStringAsync();
            var actualObj = JsonConvert.DeserializeObject<Result<string>>(model);

            result.StatusCode.Should().Be(HttpStatusCode.NotFound);
            actualObj.Should().NotBeNull();
            actualObj.Success.Should().BeFalse();
            actualObj.ResultObject.Should().BeNull();
            actualObj.SuccessMessages.Should().BeNull();
            actualObj.Errors.Count().Should().Be(1);
            actualObj.Errors[0].Message.Should().Be("There is not a file with Id: 15");
        }

        [Fact]
        public async Task Should_Remove_File_Id_23()
        {
            string id = "23";
            string path = Path.Combine(BasePath, "ServerDemo", $"{id}.txt");
            await CreateFile(path);

            HttpResponseMessage result = await TestClient.DeleteAsync($"api/file/{id}");

            var model = await result.Content.ReadAsStringAsync();
            var actualObj = JsonConvert.DeserializeObject<Result<string>>(model);

            result.StatusCode.Should().Be(HttpStatusCode.OK);
            actualObj.Should().NotBeNull();
            actualObj.Success.Should().BeTrue();
            actualObj.SuccessMessages.Count().Should().Be(1);
            actualObj.SuccessMessages[0].Should().Be("File was deleted");

            var files = Directory.GetFiles(Path.Combine(BasePath, "ServerDemo"));
            files.Any(x => x == $"{id}.txt").Should().BeFalse();
        }

        [Fact]
        public async Task Should_Return_False_In_BytesEndPoint_When_File_NotExest()
        {
            string id = "55";
            HttpResponseMessage result = await TestClient.GetAsync($"api/file/bytes/{id}");

            var model = await result.Content.ReadAsStringAsync();
            var actualObj = JsonConvert.DeserializeObject<Result<string>>(model);
            result.StatusCode.Should().Be(HttpStatusCode.NotFound);
            actualObj.Should().NotBeNull();
            actualObj.Success.Should().BeFalse();
            actualObj.SuccessMessages.Should().BeNull();
            actualObj.Errors.Count().Should().Be(1);
            actualObj.Errors[0].Message.Should().Be($"There is not a file with Id: {id}");

            var files = Directory.GetFiles(Path.Combine(BasePath, "ServerDemo"));
            files.Any(x => x == $"{id}.txt").Should().BeFalse();
        }

        [Fact]
        public async Task Should_Return_HashEndPoint_File_In_HAshPoint()
        {
            string id = "5";
            HttpResponseMessage result = await TestClient.GetAsync($"api/file/hash/{id}");

            var model = await result.Content.ReadAsStringAsync();
            var actualObj = JsonConvert.DeserializeObject<Result<string>>(model);
            result.StatusCode.Should().Be(HttpStatusCode.OK);
            actualObj.Should().NotBeNull();
            actualObj.ResultObject.Should().Be("44656D6F2054657374");
            actualObj.Success.Should().BeTrue();
        }

        [Fact]
        public async Task Should_Return_False_In_HashEndPoint_When_File_NotExest()
        {
            string id = "55";
            HttpResponseMessage result = await TestClient.GetAsync($"api/file/hash/{id}");

            var model = await result.Content.ReadAsStringAsync();
            var actualObj = JsonConvert.DeserializeObject<Result<string>>(model);
            result.StatusCode.Should().Be(HttpStatusCode.NotFound);
            actualObj.Should().NotBeNull();
            actualObj.Success.Should().BeFalse();
            actualObj.SuccessMessages.Should().BeNull();
            actualObj.Errors.Count().Should().Be(1);
            actualObj.Errors[0].Message.Should().Be($"There is not a file with Id: {id}");

            var files = Directory.GetFiles(Path.Combine(BasePath, "ServerDemo"));
            files.Any(x => x == $"{id}.txt").Should().BeFalse();
        }

        private async Task CreateFile(string path)
        {
            using (StreamWriter writer = new StreamWriter(path))
            {
                await writer.WriteLineAsync("Text");
            }
        }

        private void RemoveFile(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }
    }
}